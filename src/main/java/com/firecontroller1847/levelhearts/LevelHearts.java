package com.firecontroller1847.levelhearts;

import com.firecontroller1847.levelhearts.capabilities.IMoreHealth;
import com.firecontroller1847.levelhearts.capabilities.MoreHealth;
import com.firecontroller1847.levelhearts.gui.IngameGui;
import net.minecraft.Util;
import net.minecraft.client.Minecraft;
import net.minecraft.locale.Language;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.AttributeModifier.Operation;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.UUID;

@Mod(LevelHearts.MOD_ID)
@EventBusSubscriber(modid = LevelHearts.MOD_ID, bus = Bus.MOD)
public class LevelHearts {

    public static final String MOD_ID = "levelhearts";
    public static final String MOD_NAME = "LevelHearts";
    public static final String MOD_VERSION = "2.4.0";
    private static final UUID MODIFIER_ID = UUID.fromString("81f27f52-c8bb-403a-a1a4-b356d2f7a0f0");
    private static final Logger logger = LogManager.getLogger(LevelHearts.MOD_ID);

    public LevelHearts() {
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, Config.SPEC, "levelhearts.toml");
    }

    @SubscribeEvent
    public static void onCommonSetup(FMLCommonSetupEvent event) {
        PacketManager.register();
    }

    @SubscribeEvent
    public static void onClientSetup(FMLClientSetupEvent event) {
        MinecraftForge.EVENT_BUS.register(new IngameGui(Minecraft.getInstance()));
    }

    public static void debug(String message) {
        if (Config.enableDebug.get()) {
            logger.info(message);
        }
    }

    public static void debug(Object object) {
        debug(object.toString());
    }

    public static void error(Exception exception) {
        logger.throwing(exception);
    }

    public static void applyHealthModifier(Player player, double amount) {
        debug("LevelHearts#applyHealthModifier");
        AttributeModifier modifier = new AttributeModifier(MODIFIER_ID, "levelHearts.healthModifier", amount, Operation.ADDITION);
        AttributeInstance attribute = player.getAttribute(Attributes.MAX_HEALTH);
        attribute.removeModifier(modifier);
        attribute.addPermanentModifier(modifier);
    }

    public static void recalcPlayerHealth(Player player, int level) {
        debug("LevelHearts#calcPlayerHealth");

        // Capability, Config Files, & Local Variables
        IMoreHealth cap = MoreHealth.getFromPlayer(player);
        List<? extends Integer> ramp = Config.levelRamp.get();
        int max = Config.maxHealth.get() > 0 ? Config.maxHealth.get() : 1024;
        boolean changed = false;

        // Validate information
        if (cap.getRampPosition() < 0) {
            cap.setRampPosition((short) 0);
        }

        // Health Loop
        // Add a heart to the player if...
        // - Ramp position is less than the ramp length AND
        // - The player's level is greater than the next ramp position AND
        // - The player has not hit the maximum health
        while (cap.getRampPosition() < ramp.size() && level >= ramp.get(cap.getRampPosition()) && player.getMaxHealth() < max) {
            debug("LevelHearts#calcPlayerHealth: Adding a heart to the player.");
            changed = true;

            // Increase ramp position, add two half hearts, and notify the player
            cap.addRampPosition();
            if (Config.xpAbsorption.get()) {
                player.setAbsorptionAmount(player.getAbsorptionAmount() + 2.0f); // Add two-half absorption hearts
            } else {
                cap.setModifier(cap.getModifier() + 2); // Add two-half hearts
            }
            player.sendMessage(LevelHearts.getHeartAddedMessage((int) (player.getMaxHealth() / 2)), Util.NIL_UUID);

            // Apply the health modifier so player.getMaxHealth() returns the correct amount
            applyHealthModifier(player, cap.getTrueModifier());
        }

        // Post-loop changes
        if (changed) {
            // Notify the client of changes to the capability
            MoreHealth.updateClient((ServerPlayer) player, cap);

            // Heal the player if enabled
            if (Config.healOnLevelUp.get()) {
                player.setHealth(player.getMaxHealth());
                player.sendMessage(new TranslatableComponent("text." + MOD_ID + ".replenished"), Util.NIL_UUID);
            }

            // Remove experienced levels if enabled
            if (Config.loseXpOnLevelUp.get()) {
                if (Config.loseXpAmount.get() <= 0) {
                    player.giveExperienceLevels(-player.experienceLevel - 1);
                } else {
                    player.giveExperienceLevels(-Config.loseXpAmount.get());
                }
            }

            // Play levelup noise
            if (Config.playSoundOnHeartGain.get()) {
                player.level.playSound(null, player.getX(), player.getY(), player.getZ(), SoundEvents.PLAYER_LEVELUP, player.getSoundSource(), 0.80F, 1.0F);
            }
        }
    }

    public static TranslatableComponent getHeartAddedMessage(int hearts) {
        String key = "text." + LevelHearts.MOD_ID + ".heartadded";
        String keyHearts = key + "." + hearts;
        if (Language.getInstance().has(keyHearts)) {
            return new TranslatableComponent(keyHearts);
        } else {
            return new TranslatableComponent(key);
        }
    }

}
