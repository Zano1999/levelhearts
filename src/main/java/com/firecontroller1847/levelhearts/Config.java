package com.firecontroller1847.levelhearts;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;

import java.util.Arrays;
import java.util.List;

public class Config {

    // Config Spec
    public static final ForgeConfigSpec SPEC = Config.build();

    // General
    public static ConfigValue<Boolean> enableDebug;
    public static ConfigValue<Boolean> enableHardcore;
    public static ConfigValue<Boolean> enableItems;
    public static ConfigValue<Boolean> enableLoot;
    public static ConfigValue<Boolean> enableOhko;

    // Health
    public static ConfigValue<Integer> defHealth;
    public static ConfigValue<Integer> minHealth;
    public static ConfigValue<Integer> maxHealth;
    public static ConfigValue<List<? extends Integer>> levelRamp;
    public static ConfigValue<Boolean> playSoundOnHeartGain;
    public static ConfigValue<Integer> punishAmount;
    public static ConfigValue<Integer> healAmount;
    public static ConfigValue<Boolean> healOnLevelUp;
    public static ConfigValue<Boolean> loseXpOnLevelUp;
    public static ConfigValue<Integer> loseXpAmount;
    public static ConfigValue<Boolean> xpAbsorption;
    public static ConfigValue<Boolean> itemAbsorption;

    // Gui
    public static ConfigValue<Boolean> customHud;
    public static ConfigValue<Boolean> minimalHud;
    public static ConfigValue<Boolean> hideHud;

    // Experience
    public static ConfigValue<Double> xpMultiplier;
    public static ConfigValue<Boolean> loseXpOnDeath;
    public static ConfigValue<Boolean> loseInvOnDeath;

    private static final ForgeConfigSpec build() {
        // Create the builder
        ForgeConfigSpec.Builder builder = new ForgeConfigSpec.Builder();

        // @formatter:off
        /////////////
        // General //
        /////////////
        builder.push("General");

        enableDebug = builder
                        .comment("When enabled, an extra level of debugging output will be provided to the logs.")
                        .define("Debug", false);

        enableHardcore = builder
                            .comment("When enabled, the player's health will get reset back to default on death. Overrides punish amount.")
                            .define("Hardcore", false);

        enableItems = builder
                        .comment("When disabled, LevelHearts' items' functionality will be disabled.")
                        .define("Items", true);

        enableLoot = builder
                        .comment("When disabled, LevelHearts will not spawn heart pieces and heart containers in chests around the world.")
                        .define("Loot", true);

        enableOhko = builder
                        .comment("When enabled, upon any damage to the player, the player will immediately die. \"One-Hit Knockout\"")
                        .define("Ohko", false);

        builder.pop();

        ////////////
        // Health //
        ////////////
        builder.push("Health");

        defHealth = builder
                        .comment("The amount of health a user will have in a new game or after death.")
                        .defineInRange("Default Health", 20, 1, 200);

        minHealth = builder
                        .comment("The minimum amount of health LevelHearts will allow a user to be punished to.")
                        .defineInRange("Minimum Health", 2, 2, 20);

        maxHealth = builder
                        .comment("The maximum amount of health LevelHearts will allow a user to have. -1 to disable.")
                        .defineInRange("Maximum Health", -1, -1, 1024);

        levelRamp = builder
                        .comment("The levels at which a user will gain a heart. Cannot be zero or less than zero and cannot be more than 255.")
                        .defineList("Level Ramp", Arrays.asList(
                            1, 5, 10, 15, 20, 25, 30, 34, 38, 42,
                            46, 50, 53, 56, 59, 62, 64, 66, 68, 70,
                            75, 80, 85, 90, 95, 100, 110, 120, 130,
                            140, 150, 160, 170, 180, 190, 200
                        ), (object) -> object instanceof Integer && (Integer) object > 0 && (Integer) object < 256);

        playSoundOnHeartGain = builder
                .comment("Whether or not to play the level up XP sound each time the player receives a new heart (from the ramp or containers).")
                .define("Play Sound on Heart Gain", true);
        
        punishAmount = builder
        					.comment("How many hearts (not health) to take away from the user each time they die. -1 to disable.")
        					.defineInRange("Punish Health", -1, -1, 10);

        healAmount = builder
                        .comment("How many hearts (not health) to heal a player to each time they use a heart container. -1 for full health.")
                        .defineInRange("Heal Amount", -1, -1, 200);

        healOnLevelUp = builder
                            .comment("When disabled, the player will not be healed when they reach the next level on the level ramp.")
                            .define("Heal on Level Up", true);

        loseXpOnLevelUp = builder
                            .comment("When enabled, the player will lose some experience when they reach the next level on the level ramp.")
                            .define("Lose XP on Level Up", false);

        loseXpAmount = builder
                            .comment("The amount of experience levels to be removed from the player each time reach the next level on the level ramp. -1 to remove all levels")
                            .defineInRange("Lose XP Amount", -1, -1, 200);

        xpAbsorption = builder
                        .comment("When enabled, any hearts gained via exp will be Minecraft's absorption hearts instead of extra hearts.")
                        .define("XP Absorption", false);

        itemAbsorption = builder
                            .comment("When enabled, any hearts gained via items (heart containers) will be Minecraft's absorption hearts instead of extra hearts.")
                            .define("Item Absorption", false);

        builder.pop();

        //////////
        // Gui  //
        //////////
        builder.push("Gui");

        customHud = builder
                        .comment("When disabled, LevelHearts will not modify the hud at all. Only use if you're having conflicts with other mods.")
                        .define("LevelHearts HUD", true);

        minimalHud = builder
                        .comment("When enabled, the health will display in only one row with a number to the left indicating the row you're on.")
                        .define("LevelHearts Minimal HUD", false);

        hideHud = builder
                        .comment("When enabled, health will not be displayed at all. Works well in combination with one-hit knockout.")
                        .define("LevelHearts Hidden HUD", false);

        builder.pop();

        ////////////////
        // Experience //
        ////////////////
        builder.push("Experience");

        xpMultiplier = builder
                            .comment("How much to multiply the value of experience by.")
                            .defineInRange("XP Multiplier", 1.0, 0.0, 1024.0);

        loseXpOnDeath = builder
                            .comment("When enabled, the mod will force players to lose their experience even if the KeepInventory gamerule is enabled.")
                            .define("Always Lose XP on Death", false);

        loseInvOnDeath = builder
                                .comment("When enabled, the mod will force players to lose their items even if the KeepInventory gamerule is enabled.",
                                         "If using a setting to lose health upon death, often health will be regained through experience after respawning, which can be confusing.")
                                .define("Always Lose Inventory on Death", false);

        builder.pop();
        // @formatter:on

        // Build the spec
        return builder.build();
    }

}
