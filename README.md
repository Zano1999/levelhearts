# LevelHearts [![](https://img.shields.io/discord/173146091640848384.svg?label=Chat&colorB=2D2D2D&colorA=E04E14)](https://discord.gg/0xxkiR1rO4zRsYLp) [![](https://cf.way2muchnoise.eu/full_levelhearts_Downloads.svg)](https://minecraft.curseforge.com/projects/levelhearts) [![](https://cf.way2muchnoise.eu/versions/levelhearts.svg)](https://minecraft.curseforge.com/projects/levelhearts)

LevelHearts completely overhauls the health system. It's based off of the original "More Health Enchanted." You can gain more health with heart containers (made by heart pieces) or by gaining levels through experience. With plenty of configurable options, you can make your game experience unique to you.

No, there are no versions before 1.12.2. Please do not ask us to make them.  
Check out [More Health Enchanted](https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/1272363-1-7-10-noheros-mods-more-health-enchanted-beta) by NoHero if you need versions below 1.7.10.

## Modpack Policy
You can use this mod in your modpacks for free, no credit required (however it is preferred).